﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;


namespace Educa3
{
    
    class Program
    {
        public static void Main(string[] args)
        {
            int op = 0, id,edad,mate=0;
            string nombre, apellido, correo;
            Alumno.Agregar(23,"Pedro", "Lopez",12,"Lopez@gmail.com",2);
            Alumno.Agregar(26, "Juan", "Lavin", 23, "JL1998@gmail.com", 3);
            Alumno.Agregar(30, "Diego", "Perez", 10, "Diego.p@gmail.com", 4);
            Alumno.Agregar(28, "Carlos", "Frey", 15, "C.Frey@gmail.com", 5);
            Alumno.AgregarP(1, "Pedro", "Diaz", 33, "Pdiaz@gmail.com", 2);
            Alumno.AgregarP(2, "Juan", "Caceres", 45, "JCaceres@gmail.com", 3);
            Alumno.AgregarP(34, "Diego", "Flores", 32, "Dieg.Flo@gmail.com", 4);
            Alumno.AgregarP(5, "Carlos", "Montalva", 50, "C.MOn@gmail.com", 5);
            try
            {
                do
                {
                    
                    do {
                        Console.WriteLine("-------Eres un administrador----------" +
                                          "\n------- que deseas realizar---");
                        Console.WriteLine("-------(1) Alumnos");
                        Console.WriteLine("-------(2) Profesor");
                        Console.WriteLine("-------(3) Salir");
                        op = int.Parse(Console.ReadLine());
                    } while (op != 1 && op != 2 && op != 3);
                    switch (op)
                    {

                        case 1:
                            do
                            {

                                do
                                {

                                    Console.WriteLine("-------Elije una opcion");
                                    Console.WriteLine("-------(1) Listar Alumnos");
                                    Console.WriteLine("-------(2) Agregar Alumno");
                                    Console.WriteLine("-------(3) Eliminar Alumno");
                                    Console.WriteLine("-------(4) Modificar Alumno\n" +
                                        "---------------------------------------");

                                    Console.WriteLine("-------(5) Salir\n");
                                    op = int.Parse(Console.ReadLine());
                                } while (op != 1 && op != 2 && op != 3 && op != 4 && op != 5);
                                switch (op)
                                {
                                    case 1:
                                        Alumno.Listar();
                                        break;
                                    case 2:
                                        Console.WriteLine("------Agregar alumno-----");
                                        Console.WriteLine("Ingrese Id");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Ingrese Nombre");
                                        nombre = Console.ReadLine();
                                        Console.WriteLine("Ingrese Apellido");
                                        apellido = Console.ReadLine();
                                        Console.WriteLine("Ingrese Edad");
                                        edad = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Ingrese Correo");
                                        correo = Console.ReadLine();
                                        Console.WriteLine("Ingrese Codigo Materia \n" +
                                            "Lenguaje = 1 Matematicas = 2 Quimica = 3 Biologia = 4 Historia = 5");
                                        mate = Convert.ToInt32(Console.ReadLine());
                                        Alumno alum = new Alumno(id, nombre, apellido, edad, correo, mate);
                                        Alumno.Agregar(id, nombre, apellido, edad, correo, mate);
                                        Console.WriteLine("Nuevo Alumno agregado\n");

                                        break;
                                    case 3:
                                        Console.WriteLine("\n-------Eliminar Alumno\n------");
                                        Console.Write("Ingrese ID del Alumno para Eliminar: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Alumno.Borrar(id);
                                        break;
                                    case 4:
                                        Console.WriteLine("\nModificar Alumno\n-----------------");

                                        Console.Write("Ingrese ID Alumno: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Console.Write("Ingrese Nombre Alumno: ");
                                        nombre = Console.ReadLine().ToString();
                                        Console.Write("Ingrese Apellido Alumno: ");
                                        apellido = Console.ReadLine().ToString();
                                        Console.Write("Ingrese Edad Alumno: ");
                                        edad = Convert.ToInt32(Console.ReadLine());
                                        Console.Write("Ingrese Correo Alumno: ");
                                        correo = Console.ReadLine().ToString();
                                        Console.WriteLine("Ingrese Codigo Materia \n" +
                                            "Lenguaje = 1 Matematicas = 2 Quimica = 3 Biologia = 4 Historia = 5");
                                        mate = Convert.ToInt32(Console.ReadLine());


                                        Alumno.Modifica(id, nombre, apellido, edad, correo, mate);
                                        break;
                                  
                                    case 5:
                                        Console.WriteLine("Saliendo");
                                        break;

                                    default:
                                        Console.WriteLine("No se ingreso una opcion correcta");
                                        break;
                                }
                                Console.ReadKey();


                            } while (op != 5);
                            break;
                        case 2:
                            do
                            {

                                do
                                {

                                    Console.WriteLine("-------Elije una opcion");
                                    Console.WriteLine("-------(1) Listar Profesores");
                                    Console.WriteLine("-------(2) Agregar Profesor");
                                    Console.WriteLine("-------(3) Eliminar Profesor");
                                    Console.WriteLine("-------(4) Modificar Profesores\n" +
                                        "---------------------------------------");

                                    Console.WriteLine("-------(5) Salir\n");
                                    op = int.Parse(Console.ReadLine());
                                } while (op != 1 && op != 2 && op != 3 && op != 4 && op != 5);
                                switch (op)
                                {                                    
                                    case 1:
                                        Alumno.ListarP();
                                        break;
                                    case 2:
                                        Console.WriteLine("------Agregar Profesor-----");
                                        Console.WriteLine("Ingrese Id");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Ingrese Nombre");
                                        nombre = Console.ReadLine();
                                        Console.WriteLine("Ingrese Apellido");
                                        apellido = Console.ReadLine();
                                        Console.WriteLine("Ingrese Edad");
                                        edad = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Ingrese Correo");
                                        correo = Console.ReadLine();
                                        Console.WriteLine("Ingrese Codigo Materia \n" +
                                            "Lenguaje = 1 Matematicas = 2 Quimica = 3 Biologia = 4 Historia = 5");
                                        mate = Convert.ToInt32(Console.ReadLine());
                                        Alumno profe = new Alumno(id, nombre, apellido, edad, correo, mate);
                                        Alumno.AgregarP(id, nombre, apellido, edad, correo, mate);
                                        Console.WriteLine("Nuevo Profesor agregado\n");

                                        break;
                                    case 3:
                                        Console.WriteLine("\n-------Eliminar Profesor\n------");
                                        Console.Write("Ingrese ID del Profesor para Eliminar: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Alumno.BorrarP(id);
                                        break;
                                    case 4:
                                        Console.WriteLine("\nModificar Profesor\n-----------------");

                                        Console.Write("Ingrese ID Profesor: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        Console.Write("Ingrese Nombre Profesor: ");
                                        nombre = Console.ReadLine().ToString();
                                        Console.Write("Ingrese Apellido Profesor: ");
                                        apellido = Console.ReadLine().ToString();
                                        Console.Write("Ingrese Edad Profesor: ");
                                        edad = Convert.ToInt32(Console.ReadLine());
                                        Console.Write("Ingrese Correo Profesor: ");
                                        correo = Console.ReadLine().ToString();
                                        Console.WriteLine("Ingrese Codigo Materia \n" +
                                            "Lenguaje = 1 Matematicas = 2 Quimica = 3 Biologia = 4 Historia = 5");
                                        mate = Convert.ToInt32(Console.ReadLine());


                                        Alumno.ModificaP(id, nombre, apellido, edad, correo, mate);
                                        break;
                                    case 5:
                                        Console.WriteLine("Saliendo");
                                        break;

                                    default:
                                        Console.WriteLine("No se ingreso una opcion correcta");
                                        break;
                                }
                                Console.ReadKey();


                            } while (op != 5);

                            break;

                        case 3:
                            Console.WriteLine("Saliendo");
                         
                            break;
                            
                    }
                    Console.ReadKey();
                } while (op!=3);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
