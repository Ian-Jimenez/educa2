﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;


namespace Educa3
{
    class Alumno
    {
        public static List<Alumno> alumno = new List<Alumno>();
        public static List<Alumno> profesor = new List<Alumno>();
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int edad { get; set; }
        public string correo { get; set; }
        public int mate { get; set; }

        public Alumno(int id, String nombre, String apellido, int edad, String correo,int mate)
        {
            this.id = id;
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.correo = correo;
            this.mate = mate;
        }
        enum Materia 
        { 
            Lenguaje = 1,
            Matematicas = 2,
            Quimica = 3,
            Biologia = 4,
            Historia = 5
        }


        public static void Agregar(int id, String nombre, String apellido, int edad, String correo,int mate)
        {
            if (!Verifica(id))
            {
                alumno.Add(new Alumno(id, nombre, apellido, edad, correo, mate));
                
            }
            else
            {
                Console.WriteLine("Alumno ya existe");
            }
        }
        
        private static bool Verifica(int id)
        {
            foreach (var v in alumno)
            {
                if (id.Equals(v.id))
                {
                    return true;
                }
            }
            return false;
        }
        public static void Listar()
        {
            Console.WriteLine("-----lista Alumnos------");
            Console.WriteLine($"ID:-----Nombre: -------Apellido:---------Correo:--------------------Edad:--------------Materia:");
            alumno =alumno.OrderBy(a => a.id).ToList();
            foreach (var a in alumno)
            {
                Console.WriteLine($"\nID:- {a.id}-Nombre:- {a.nombre}-Apellido:- {a.apellido}-Correo:- {a.correo}-Edad:- {a.edad}\n");
                

                if (a.mate.Equals(1))
                {
                    Console.WriteLine("Materia: " + ((Materia)1));
                }
                else if (a.mate.Equals(2))
                {
                    Console.WriteLine("Materia: " + ((Materia)2));
                }
                else if (a.mate.Equals(3))
                {
                    Console.WriteLine("Materia: " + ((Materia)3));
                }
                else if (a.mate.Equals(4))
                {
                    Console.WriteLine("Materia: " + ((Materia)4));
                }
                else if (a.mate.Equals(5))
                {
                    Console.WriteLine("Materia: " + ((Materia)5));
                }
                else
                {
                    Console.WriteLine("--no corresponde a materia");
                }
                               
            }
        }
        public static void Modifica(int id, string nombre, string apellido, int edad, string correo,int mate)
        {
            if (NotNull())
            {
                foreach (var m in alumno)
                {
                    if (id.Equals(m.id))
                    {
                        Console.Write($"Nombre: {m.nombre}\n apellido: {m.apellido}\n Edad: {m.edad}\n Correo: {m.correo}\n Materia: { m.mate}\n");
                        m.nombre = nombre;
                        m.apellido = apellido;
                        m.edad = edad;
                        m.correo = correo;
                        m.mate = mate;
                    }
                    else
                    {
                        Console.WriteLine("Alumno no Encontrado");
                    }
                }
                Console.WriteLine("Alumno Modificado!\n");
            }
        }
        public static void Borrar(int id)
        {
            if (NotNull())
            {
                if (Verifica(id))
                {
                    foreach (var u in alumno)
                    {
                        if (id.Equals(u.id))
                        {
                            alumno.Remove(u);
                            Console.WriteLine("Alumno Eliminado!\n");
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Alumno No Existe! \n");
                }
            }
        }
        public static void AgregarP(int id, String nombre, String apellido, int edad, String correo,int mate)
        {
            if (!Verifica(id))
            {
                profesor.Add(new Alumno(id, nombre, apellido, edad, correo, mate));
                
            }
            else
            {
                Console.WriteLine("Profesor ya existe");
            }
        }
       
        public static void ListarP()
        {
            Console.WriteLine("-----lista Profesor------");
            Console.WriteLine($"ID:-----Nombre: -------Apellido:---------Correo:--------------------Edad:-------------Materia:-");

            profesor = profesor.OrderBy(l => l.id).ToList();
            foreach (var l in profesor)
            {
                
                Console.WriteLine($"\n ID: {l.id}-Nombre:- {l.nombre}-Apellido:- {l.apellido}-Correo:- {l.correo}-Edad:- {l.edad}\n");

                if (l.mate.Equals(1))
                {
                    Console.WriteLine("Materia: " + ((Materia)1));
                }
                else if (l.mate.Equals(2))
                {
                    Console.WriteLine("Materia: " + ((Materia)2));
                }
                else if (l.mate.Equals(3))
                {
                    Console.WriteLine("Materia: " + ((Materia)3));
                }
                else if (l.mate.Equals(4))
                {
                    Console.WriteLine("Materia: " + ((Materia)4));
                }
                else if (l.mate.Equals(5))
                {
                    Console.WriteLine("Materia: " + ((Materia)5));
                }
                else
                {
                    Console.WriteLine("--no corresponde a materia");
                }
            }
        }
        public static void ModificaP(int id, string nombre, string apellido, int edad, string correo,int mate)
        {
            if (NotNullp())
            {
                foreach (var m in profesor)
                {
                    if (id.Equals(m.id))
                    {
                        Console.Write($"Nombre: {m.nombre}\n apellido: {m.apellido}\n Edad: {m.edad}\n Correo: {m.correo}\nMateria: { m.mate}\n");
                        m.nombre = nombre;
                        m.apellido = apellido;
                        m.edad = edad;
                        m.correo = correo;
                        m.mate = mate;
                    }
                    else
                    {
                        Console.WriteLine("Profesor no Encontrado");
                    }
                }
                Console.WriteLine("Profesor Modificado!\n");
            }
        }
        public static void BorrarP(int id)
        {
            if (NotNullp())
            {
                if (Verifica(id))
                {
                    foreach (var u in profesor)
                    {
                        if (id.Equals(u.id))
                        {
                            profesor.Remove(u);
                            Console.WriteLine("Profesro Eliminado!\n");
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Profesor No Existe!\n");
                }
            }
        }
        private static bool NotNull()
        {
            if (alumno.Count > 0)
            {
                return true;
            }
            else
            {
                Console.WriteLine("\nIngrese un Alumno Primero\n");
                return false;
            }
        }
        private static bool NotNullp()
        {
            if (profesor.Count > 0)
            {
                return true;
            }
            else
            {
                Console.WriteLine("\nIngrese un Profesor Primero\n");
                return false;
            }
        }

    }
}
